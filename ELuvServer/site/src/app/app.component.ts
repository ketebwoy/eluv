import { Component } from '@angular/core';
import { FireBaseData } from './services/firebase.service';
import { Subscription } from 'rxjs/Subscription';
import { Observable } from "rxjs/Observable";
import { Http, Headers, Response, BrowserXhr, RequestOptions } from "@angular/http";
import { UserInfo } from './models/user.model';
import { TimerObservable } from "rxjs/observable/TimerObservable";
import * as moment from 'moment';
import { ShareTypes } from './models/sharetypes';
import { SharingService } from './services/sharing.service';
import 'rxjs/add/operator/map';


// firebase
declare var firebase: any;
declare var ses: any;


//declare var moment: any;

@Component({
  selector: 'my-app',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [SharingService]

})

export class AppComponent {
  private accountSid = 'ACb4ffcf75319a3a456f6e0b4e8db13df7';
  private authToken = '<c0c7759184eb9a7fcc153287ccb27d19>';
  private aws_access_key_id = 'AKIAI472TXJOR7ZE7Z6A';
  private aws_secret_access_key = '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';


  public record: any;
  fbd: any;
  //share: any; 
  public recorddata: any;
  private foo: any;
  private bar: any;
  private fname: any = "Fetching your record file";
  private user: any;
  private username: any;
  private contacts: any;
  options: string[];
  shareTypes: any;
  //private _isUserLoggedIn = new BehaviorSubject<boolean>(false);
  //private _user = new BehaviorSubject<UserInfo>(new UserInfo(null, null, null, null, null, null, null, null, null));

  // private _company = new BehaviorSubject<CompanyInfo>(new CompanyInfo(null, null, null, null, null, null, null, null, null, null, null, null));
  tick: any;
  private _timingobserver: any;
  private _userobserver: any;
  private subscription: any;
  private s3: any;
  private url: any;
  private schedule:  {src: any, to: any, datecreated: any, datemodified: any, processed: any, scheduleid: any};
  audio: any; 
  song: any; 
  title: any; 
  artist: any;
  src: any; 


  constructor(private _http: Http) {

    this.url = 'http://tinyurl.com/hz27ogr';
    this.user = new UserInfo(null, null, null, null, null, null, null, null, null, null);
    this.fbd = new FireBaseData;
    this.shareTypes = new ShareTypes;
    //  this.share = new SharingService(this._http);    
    var url = window.location.href;
    var scheduleid = this.getQueryString('sid', url);
    //var userid = this.getQueryString("us", url);
    //alert("userid" + userid); 

    this.fbd.getschedule(scheduleid).then((scheduledata: any) => {
      this.schedule = scheduledata;
   //   alert(JSON.stringify(scheduledata));

      var songdata = this.getsongdata(scheduledata.src);
     // alert('songdata' + JSON.stringify(songdata));
      this.song = songdata[0]; 
      
      this.artist = this.song.artist;
      this.title = this.song.title; 
      this.src = this.schedule.src;

      this.audio = new Audio();
      this.audio.src = this.src;
      this.audio.load(); 
      //  this.fbd.getuser(recdata.userid).then((user: any) => {
      // //    alert("Got user: " + JSON.stringify(user));
      // this.user = user;
      // this.username = this.user.fullname;
      //  });


      // this.fbd.getdownloadurl(recdata.filename).then((url: any) => {
      //   // alert("the download url is: " + url);
      //   this.record = url;
      // });

    });


   



  }


  getQueryString(field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
  };

  private handleError(error: Response) {
    alert('Failed to fetch records');
    return Observable.throw(error.json().error || 'Server Error');
  }


  getsongdata(songdata) {
      var filt = []; 
       var tracks = [{
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FAPB%20MP3.wav?alt=media&token=22c6f2ea-d5bf-4585-bbbe-307aabcd1363',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FAPB%20MP3.mp3?alt=media&token=3a4dbc5a-eb6a-4cf8-81ab-da9e454e4835', 
      artist: 'Ken Washington',
      title: 'Absolutely Positively Beautiful',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
      tags: ['valentines'] // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
      
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMy%20Music.wav?alt=media&token=efcf735f-9208-47ad-810c-9d170e06fa36',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Music.mp3?alt=media&token=aeb156ab-ae36-44a5-81b1-720f8947dc12', 
      artist: 'Ken Washington',
      title: 'My Music',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
      tags: ['wedding', 'mothers'] // tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
    }, 
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FReal%20MP3.mp3?alt=media&token=7a42ed06-f974-4064-94c3-3307f21f5346',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FReal%20MP3.mp3?alt=media&token=fb731d4c-eb41-49e5-a5d6-f183a2893d6b', 
      artist: 'Ken Washington',
      title: 'Real',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
       tags: ['mothers', 'valentines'] // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    },
     {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMyDesire.mp3?alt=media&token=4d2e2897-200b-481b-92ac-52309885034c',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Desire%20MP3.mp3?alt=media&token=1fc812ad-2f1f-4221-864d-106bfe0e6ad8', 
      artist: 'Ken Washington',
      title: 'My Desire',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
       tags: ['valentines', 'wedding'] // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    }];


    tracks.forEach(function(item, index) {
      if (item.fullsrc == songdata) {
        filt.push(item);
      }
      
    });
    return filt;
  }

   play() {
        this.audio.play();
    }

    pause() {
        this.audio.pause();
    }

    stop() {
        this.audio.pause();
        this.audio.currentTime = 0;
    }



}
