export class UserInfo  {
  constructor(
    public datecreated: string,
    public datemodified: string,
    public email: string,
    public phone: string,
    public fullname: string,
    public profilepic: string,
    public companyid: string,
    public userid: string,
    public accountid: string,
    public role: string
  ) {
    if (datecreated = null)
    {
      this.datecreated = new Date().toUTCString(); 
      this.datemodified = new Date().toUTCString(); 
    }
   }
};