export class CompanyInfo  {
  constructor(
    public datecreated: string,
    public datemodified: string,
    public address: string,
    public city: string,
    public zipcode: string,
    public state: string, 
    public email: string,
    public phone: string,
    public companyname: string,
    public owneraccountid: string,
    public companyid: string,
    public logo: string
  ) {
     if (datecreated = null)
    {
      this.datecreated = new Date().toUTCString(); 
      this.datemodified = new Date().toUTCString(); 
    }
   }
};