 import {Injectable} from "@angular/core";
 import {BehaviorSubject} from 'rxjs/BehaviorSubject';
 import {Observable} from 'rxjs/Observable';
 import {Observer} from 'rxjs/Observer';
 import 'rxjs/add/operator/share';
 import {UserInfo} from  '../models/user.model';
import {CompanyInfo} from  '../models/company.model';



 @Injectable() export class AppGlobals {
   // use this property for property binding
   private _isUserLoggedIn = new BehaviorSubject<boolean>(false);
 
   private _user = new BehaviorSubject<UserInfo>(new UserInfo(null, null, null, null, null, null, null, null, null, null));
 
   private _company = new BehaviorSubject<CompanyInfo>(new CompanyInfo(null, null, null, null, null, null, null, null, null, null, null, null));


//    loggedinstatus$ = this._isUserLoggedIn.asObservable();
//    user$ = this._user.asObservable();
//    company$ = this._company.asObservable();
 

//    constructor() {
//     //    this.loggedinstatus$ = new Observable(observer => this._observer = observer).share();
//    }



//    setloginstatus(isLoggedIn: boolean){
//        this._isUserLoggedIn.next(isLoggedIn);
//   }

//    setcompany(companyarg: any){
//        this._company.next(companyarg);
//    }

//    setuser(userarg: any) {
//        this._user.next(userarg);
//    }

  

}