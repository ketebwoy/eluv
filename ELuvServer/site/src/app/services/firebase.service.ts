import { Injectable } from '@angular/core';
import { UserInfo } from '../models/user.model';
import { CompanyInfo } from '../models/company.model';
import * as moment from 'moment';


declare var firebase: any;

@Injectable()
export class FireBaseData {

  public user: any;
  public recordsdata: any;
  public userdata: any;
  public menudata: any;
  public alertMessage: any;
  public recordstorage: any;
  public userrecs: any;
  public usersettings: any;
  public schedulesfb: any;


  constructor() {
    var rec = firebase.storage();
    this.user = new UserInfo(null, null, null, null, null, null, null, null, null, null);
    var recref = rec.ref();
    this.recordstorage = recref.child("record/");
    this.userdata = firebase.database().ref('users');
    this.userrecs = firebase.database().ref('records');
    this.usersettings = firebase.database().ref('settings');
    this.schedulesfb = firebase.database().ref('schedules');
  }



  getschedule(scheduleid: any) {
    //  alert("made it to getuserrecord with parameters: " + JSON.stringify(recordid));
    return new Promise((resolve, reject) => {
      this.schedulesfb.child(scheduleid).once("value", function (snapshot: any) { //.equalTo(company.companyid).once("value", function (snapshot: any) {
        var scheduledata = snapshot.child("/").val();

        resolve(scheduledata);
      });
    });
  }
  

  
   updateschedule(songschedule) {
    // Save user profile     
   // alert('update user settings: ' + JSON.stringify(settings));
    songschedule.datemodified = new Date().toUTCString();
    this.schedulesfb.child(songschedule.scheduleid).update(songschedule);
  }

}