import {Component} from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import * as moment from 'moment';  
import { FireBaseData } from '../services/firebase.service';

@Component(
     { 
         selector: 'page-modal',
         templateUrl: 'purchasemodal.html'}
     )
     
export class PurchaseModal {
passedin: any; 
myDate: any =  new Date().toISOString(); 
contact: any; 
ct: any;
fbd: any; 
min: any; 

 constructor(params: NavParams, private viewCtrl: ViewController) {
  this.passedin = params.get('track');
  this.ct = 1; 
  this.fbd = new FireBaseData();
  this.min = moment().format('LLLL');
 }

 dismiss() {
     this.viewCtrl.dismiss(false);
 }

 save() {
    this.viewCtrl.dismiss(true);  
  
  
 }


}
