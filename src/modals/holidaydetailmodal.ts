import {Component} from '@angular/core';
import { NavController, NavParams, ViewController, ModalController } from 'ionic-angular';
import { DatePicker } from 'ionic-native';
import * as moment from 'moment';  
import { FireBaseData } from '../services/firebase.service';
import { PurchaseModal } from '../modals/purchasemodal';
import  {Artists } from '../pages/artist/artist';

@Component(
     { 
         selector: 'page-modal',
         templateUrl: 'holidaydetailmodal.html'}
     )
export class HolidayDetailModal {
passedin: any; 
myDate: any =  new Date().toISOString(); 
contact: any; 
ct: any;
fbd: any; 
min: any; 
filteredtracks: Array<{  src: string, fullsrc: string, artist: string,  title: string,  art: string,   preload: string,  tags: string[], purchased: boolean }>;
myTracks      : Array<{  src: string, fullsrc: string, artist: string,  title: string,  art: string,   preload: string,  tags: string[], purchased: boolean }>;
allTracks: any[];
selectedTrack: number;
arr: any; 
tags: string[];
icon: any;

 constructor(public navCtrl: NavController, params: NavParams, private viewCtrl: ViewController, public modalCtrl: ModalController) {
  this.passedin = params.get('holiday');
  this.icon = params.get('icon');
  //alert("passedin value: " + JSON.stringify(this.passedin) + ':' + JSON.stringify(this.icon))
  this.ct = 1; 
  this.fbd = new FireBaseData();
  this.min = moment().format('LLLL');

  this.myTracks = [{
    src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FAPB%20MP3.wav?alt=media&token=22c6f2ea-d5bf-4585-bbbe-307aabcd1363',
    fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FAPB%20MP3.mp3?alt=media&token=3a4dbc5a-eb6a-4cf8-81ab-da9e454e4835', 
    artist: 'Ken Washington',
    title: 'Absolutely Positively Beautiful',
    art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
    preload: 'metadata',
    tags: ['valentines', 'birthday'],
    purchased: false // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    
  },
  {
    src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMy%20Music.wav?alt=media&token=efcf735f-9208-47ad-810c-9d170e06fa36',
    fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Music.mp3?alt=media&token=aeb156ab-ae36-44a5-81b1-720f8947dc12', 
    artist: 'Ken Washington',
    title: 'My Music',
    art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
    preload: 'metadata',
    tags: ['wedding', 'just because'] ,
    purchased: false// tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
  }, 
  {
    src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FReal%20MP3.mp3?alt=media&token=7a42ed06-f974-4064-94c3-3307f21f5346',
    fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FReal%20MP3.mp3?alt=media&token=fb731d4c-eb41-49e5-a5d6-f183a2893d6b', 
    artist: 'Ken Washington',
    title: 'Real',
    art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
    preload: 'metadata',
     tags: ['just because', 'valentines'] ,
    purchased: false
     // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
  },
   {
    src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMyDesire.mp3?alt=media&token=4d2e2897-200b-481b-92ac-52309885034c',
    fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Desire%20MP3.mp3?alt=media&token=1fc812ad-2f1f-4221-864d-106bfe0e6ad8', 
    artist: 'Ken Washington',
    title: 'My Desire',
    art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
    preload: 'metadata',
     tags: ['valentines', 'wedding'],
    purchased: false // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
  }];

  this.filteredtracks = this.myTracks;
  this.getItems(this.passedin)

 }

 dismiss() {
     this.viewCtrl.dismiss(false);
 }

 save() {

 }

 initializeItems() {
    this.tags = ['wedding', 'valentines', 'just because', 'birthday'];

    if(this.filteredtracks.length > 0 ) {
   //   alert("made it inside if");
      var counter = 0; 
      this.filteredtracks.forEach(element => {
        counter++; 
     //   alert("saving purchase information for track: " +  counter + element.purchased); 
      });
    }

    this.filteredtracks = this.myTracks;
  
  }

  getItems(holiday: any) {
    // Reset items back to all of the items
   // alert('event params: ' + JSON.stringify(ev));
    this.initializeItems();

    // set val to the value of the searchbar
    let val = holiday.toString();
  //  alert("holiday value: " + JSON.stringify(holiday))
    var filt = []; 
    this.myTracks.forEach(function(item, index) {
      var count = 0; 
      item.tags.forEach(function(item, index) {
    //    alert('holiday = ' + JSON.stringify(holiday) + ' : ' + item);
        
        if(val.toLowerCase().includes(item.toLowerCase())) {
          count++; 
        }
      });

      if(count > 0) {
        filt.push(item);
      }
      
    });

   // alert('filtered songs' + JSON.stringify(filt));
    
    if(filt.length > 0) {
       this.filteredtracks = filt; 
    }
    else {
      this.filteredtracks = this.myTracks; 
    }
   


    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.tags = this.tags.filter((item) => {
    //     this.filteredsnippetags = this.filtersnippets(val);
    //     return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    // }
  }

  
  purchasemodal(track: any) {
   
    return new Promise((resolve, reject) => {
         //  alert('load modal w/ params: ' + JSON.stringify(track));
          let purchModal = this.modalCtrl.create(PurchaseModal, { track: track });
          purchModal.onDidDismiss(data => {
            if(data) {
              resolve(data);
            }          
          }); 
          purchModal.present();

      });
  }

  
  purchase(track: any ) {
    this.purchasemodal(track).then((data: any) => {
      if(data) {
          track.purchased = true; 
      var elem = this.myTracks.findIndex(p => p.src === track.src);
   //  alert("purchase simulation for track "+ track.title + JSON.stringify(elem));
      return true; 
      }
    });
  
  }

  gotoArtist(artist: any) {
    alert(JSON.stringify(artist));
    this.navCtrl.push(Artists);
  }
   
}