import {Component} from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { DatePicker } from 'ionic-native';
import * as moment from 'moment';  
import { FireBaseData } from '../services/firebase.service';

@Component(
     { 
         selector: 'page-modal',
         templateUrl: 'sendmodal.html'}
     )
export class SendModal {
passedin: any; 
myDate: any =  new Date().toISOString(); 
contact: any; 
ct: any;
fbd: any; 
min: any; 

 constructor(params: NavParams, private viewCtrl: ViewController) {
  this.passedin = params.get('track');
  this.ct = 1; 
  this.fbd = new FireBaseData();
  this.min = moment().format('LLLL');
 }

 dismiss() {
     this.viewCtrl.dismiss(false);
 }

 save() {
    
     var datecreated =  moment().format('LLLL');
     var datemodified = moment().format('LLLL');
     var senddt =  moment(this.myDate).format('LLLL');
     var contact = this.contact;
     var contype = this.ct; 
     var src = this.passedin.fullsrc; 
     var processed = false;
     var schedule = {src: src, to: contact, type: contype, sendtime: senddt, datecreated: datecreated, datemodified: datemodified, processed: processed, scheduleid: ""};

     this.fbd.createsongschedule(schedule).then((data: any) => {
     alert('schedule data saved: ' + data.key);

     // this.fbd.updateuser(this.newuser);

      if (data.key != null) {
      //    alert('settings data key:' + data.key)
            schedule.scheduleid = data.key;
            this.fbd.updateschedule(schedule);
            this.viewCtrl.dismiss(true);
            alert('scheduled id updated : ' + JSON.stringify(schedule.scheduleid));
      }
    });

   
  
 }

pickdate() {
    DatePicker.show({
  date: new Date(),
  mode: 'date'
}).then(
  date => console.log('Got date: ', date),
  err => console.log('Error occurred while getting date: ', err)
);
}


}
