import {Component} from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { DatePicker } from 'ionic-native';
import * as moment from 'moment';  
import { FireBaseData } from '../../services/firebase.service';

@Component(
     { 
         selector: 'page-artist',
         templateUrl: 'artist.html'}
     )

     
export class Artists {
  passedin: any; 
  myDate: any =  new Date().toISOString(); 
  contact: any; 
  ct: any;
  fbd: any; 
  min: any; 
  filteredtracks: Array<{  src: string, fullsrc: string, artist: string,  title: string,  art: string,   preload: string,  tags: string[], purchased: boolean }>;
  myTracks      : Array<{  src: string, fullsrc: string, artist: string,  title: string,  art: string,   preload: string,  tags: string[], purchased: boolean }>;
  allTracks: any[];
  selectedTrack: number;
  arr: any; 
  tags: string[];
  icon: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.myTracks = [{
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FAPB%20MP3.wav?alt=media&token=22c6f2ea-d5bf-4585-bbbe-307aabcd1363',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FAPB%20MP3.mp3?alt=media&token=3a4dbc5a-eb6a-4cf8-81ab-da9e454e4835', 
      artist: 'Ken Washington',
      title: 'Absolutely Positively Beautiful',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
      tags: ['valentines', 'birthday'],
      purchased: false // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
      
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMy%20Music.wav?alt=media&token=efcf735f-9208-47ad-810c-9d170e06fa36',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Music.mp3?alt=media&token=aeb156ab-ae36-44a5-81b1-720f8947dc12', 
      artist: 'Ken Washington',
      title: 'My Music',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
      tags: ['wedding', 'just because'] ,
      purchased: false// tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
    }, 
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FReal%20MP3.mp3?alt=media&token=7a42ed06-f974-4064-94c3-3307f21f5346',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FReal%20MP3.mp3?alt=media&token=fb731d4c-eb41-49e5-a5d6-f183a2893d6b', 
      artist: 'Ken Washington',
      title: 'Real',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
       tags: ['just because', 'valentines'] ,
      purchased: false
       // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    },
     {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMyDesire.mp3?alt=media&token=4d2e2897-200b-481b-92ac-52309885034c',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Desire%20MP3.mp3?alt=media&token=1fc812ad-2f1f-4221-864d-106bfe0e6ad8', 
      artist: 'Ken Washington',
      title: 'My Desire',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
       tags: ['valentines', 'wedding'],
      purchased: false // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    }];
  
    this.filteredtracks = this.myTracks;
    
  }
}