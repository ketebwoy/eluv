import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
//import { TabsPage } from '../tabs/tabs';
import { HelloIonicPage } from '../hello-ionic/hello-ionic';
import { UserInfo } from '../../models/user.model';
import { FireBaseData } from '../../services/firebase.service';
import { AppGlobals } from '../../services/globals';
import { LocalStorage } from '../../services/storage';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {
 passedin: any; 
 showcreate: any; 
 newuser = new UserInfo(null, null, null, null, null, null, null, null, null);
 tempnewuser: any;
 password: any;
 showsignin: any; 
 fbd: any;
 localstorage: any;
 error: any;  

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private _globals: AppGlobals, public storage: Storage) {
    this.showcreate = true;      
    this.fbd = new FireBaseData();
    this.localstorage = new LocalStorage(storage);
    this.error = "nothing";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  login(){
    var credentials = { email: this.newuser.email, password: this.password }
    //Api connections
    alert('login creentials: ' + JSON.stringify(credentials));
    this.navCtrl.push(HelloIonicPage); 

    // this.fbd.login(credentials).then((user: any) => {
    //   alert('got user: ' + JSON.stringify(user));
    //   this.navCtrl.push(HelloIonicPage); 
    //   // if (user.uid) {
    //   //   alert(JSON.stringify(user.uid));
    //   //   this.fbd.getuserbyaccount(user.uid).then(user => {
    //   //     if(user) {
    //   //       alert('got user: ' + JSON.stringify(user));
    //   //       this.newuser = user; 
    //   //       //this._globals.setuser(this.newuser);
    //   //      // this.localstorage.setaccount(this.newuser);
    //   //      // this.viewCtrl.dismiss({user: this.newuser, go2settings: false});       
    //   //      this.navCtrl.push(HelloIonicPage); 
    //   //     }
    //   //   }).catch(function (error: any) {
    //   //    alert(JSON.stringify(error));
    //   //   });
       
        
    //   // }
    // }).catch(function (error: any) {
    //       alert(JSON.stringify(error));
    //     });

  
    }

}
