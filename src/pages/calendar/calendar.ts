import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
//import { TabsPage } from '../tabs/tabs';
import { HelloIonicPage } from '../hello-ionic/hello-ionic';
import { UserInfo } from '../../models/user.model';
import { FireBaseData } from '../../services/firebase.service';
import { AppGlobals } from '../../services/globals';
import { LocalStorage } from '../../services/storage';
import { Storage } from '@ionic/storage';
/**

/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class Calendar {
  passedin: any; 
 showcreate: any; 
 newuser = new UserInfo(null, null, null, null, null, null, null, null, null);
 tempnewuser: any;
 password: any;
 showsignin: any; 
 fbd: any;
 localstorage: any;
 error: any;  
 subscriptionLevel: any;
 mode: Boolean = false; 
 currentEvents:  Array<{ year: string, month: string, date: string }>; 

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private _globals: AppGlobals, public storage: Storage) {
    if(navParams.get('data')) {
      this.newuser = navParams.get('data');
    }
    
    this.showcreate = true;      
    this.fbd = new FireBaseData();
    this.localstorage = new LocalStorage(storage);
    this.error = "nothing";
    this.newuser.profilepic = 'http://www.gravatar.com/avatar?d=mm&s=140';
    
    this.currentEvents = [
      {
        year: "2018",
        month: "08",
        date: "25"
      },
      {
        year: "2018",
        month: "08",
        date: "15"
      }
    ];

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

  goHome() {
      this.navCtrl.push(HelloIonicPage);
  }

  editMode(mode: any) {
      if(mode) {
        alert("edit mode on")
        mode = false; 
      }
      else {
        alert("edit mode off")
        mode = true; 
      }
  }

  updateAvatar() {
    alert("trying to update");
  }

  onDaySelect(event: any) {
    alert(JSON.stringify(event));
  }

  onMonthSelect(event: any) {
    alert(JSON.stringify(event));
  }
   

}
