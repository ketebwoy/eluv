import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
//import { TabsPage } from '../tabs/tabs';
import { Account } from '../account/account';
import { UserInfo } from '../../models/user.model';
import { FireBaseData } from '../../services/firebase.service';
import { AppGlobals } from '../../services/globals';
import { LocalStorage } from '../../services/storage';
import { Storage } from '@ionic/storage';
/**

/**
 * Generated class for the Signup page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class Signup {
  passedin: any; 
 showcreate: any; 
 newuser = new UserInfo(null, null, null, null, null, null, null, null, null);
 tempnewuser: any;
 password: any;
 showsignin: any; 
 fbd: any;
 localstorage: any;
 error: any;  

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private _globals: AppGlobals, public storage: Storage) {
    this.showcreate = true;      
    this.fbd = new FireBaseData();
    this.localstorage = new LocalStorage(storage);
    this.error = "nothing";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Signup');
  }

  signup(){
    var credentials = { email: this.newuser.email, password: this.password }
   alert("credentials: " + JSON.stringify(credentials));
   this.navCtrl.push(Account, {
    data: this.newuser
  });     

  // var nu: any;
  //   this.fbd.createaccount({ email: this.newuser.email, password: this.password }).then((user: any) => {
  //    alert("created User " + JSON.stringify(user))
  //       if (user.uid.Length > 0) {

  //       //create a user object
  //       this.newuser.role = "user";
  //       this.newuser.accountid = user.uid;
  //       this.newuser.profilepic = 'http://www.gravatar.com/avatar?d=mm&s=140';
  //       this.fbd.createuser(this.newuser).then((user: any) => {

  //         if (user.key != null) {
  //           this.newuser.userid = user.key;
  //           this.fbd.updateuser(this.newuser);
  //              //  this._globals.setuser(this.newuser);
  //          // this.localstorage.setaccount(this.newuser);
  //          // this.viewCtrl.dismiss({user: this.newuser, go2settings: false});  
  //           //Api connections
  //           this.navCtrl.push(Account);      
  //         }
  //         else {
  //           alert("an error occured creating user, please try again");
  //         }
  //       });
  //     }
  //   });
  }

   

}
