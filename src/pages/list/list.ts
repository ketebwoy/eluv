import { Component } from '@angular/core';
import { NavController, NavParams, ModalController } from 'ionic-angular';
import { ItemDetailsPage } from '../item-details/item-details';
import { FireBaseData } from '../../services/firebase.service';
import async from 'async-es';
import { AudioProvider } from 'ionic-audio';
import { SendModal } from '../../modals/sendmodal'; 
import { PurchaseModal } from '../../modals/purchasemodal';
import { HolidayDetailModal } from '../../modals/holidaydetailmodal';
import * as wait from 'wait.for';







@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: any;
  icons: string[];
  items: Array<{ title: string, note: string, icon: string }>;
  tags: string[];
  songs: Array<{ title: string, tags: string[] }>;
  songdataarr: Array<{ filename: string, tags: string, url: string }>;
  songdata: { filename: string, tags: string, url: string };
  snippetags: Array<{ filename: string, tags: string, url: string }>;
  filteredsnippetags: Array<{ filename: string, tags: string, url: string }>;
  select: any;
  fbd: any;
  filteredtracks: Array<{  src: string, fullsrc: string, artist: string,  title: string,  art: string,   preload: string,  tags: string[], purchased: boolean }>;
  myTracks      : Array<{  src: string, fullsrc: string, artist: string,  title: string,  art: string,   preload: string,  tags: string[], purchased: boolean }>;
  allTracks: any[];
  selectedTrack: number;
  arr: any; 
  holidayItems: any; 
  images: Array<string>;  
  holidays: Array<string>;
  grid: Array<Array<any>>; //array of arrays
  counter: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private _audioProvider: AudioProvider, public modalCtrl: ModalController) {
    // If we navigated to this page, we will have an item available as a nav param
    this.fbd = new FireBaseData();
    this.selectedItem = navParams.get('item');

    this.holidayItems = [
      {
        holiday: "Just Because",
        avatarUrl: "/assets/img/GENERAL.jpg"
      },
      {
        holiday: "Birthday",
        avatarUrl: "/assets/img/birthday.jpg"
      },
      {
        holiday: "Valentines",
        avatarUrl: "/assets/img/valentines.jpg"
      },
      {
        holiday: "Anniversary Wedding",
        avatarUrl: "/assets/img/WeddingAnniversary.jpg"
      },
      {
        holiday: "Easter",
        avatarUrl: "/assets/img/Easter.jpg"
      },
      {
        holiday: "Fathers Day",
        avatarUrl: "/assets/img/fathersday.jpg"
      },
      {
        holiday: "Mothers Day",
        avatarUrl: "/assets/img/mothersday.jpg"
      },
      {
        holiday: "Halloween",
        avatarUrl: "/assets/img/halloween.jpg"
      }];
      


    this.myTracks = [{
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FAPB%20MP3.wav?alt=media&token=22c6f2ea-d5bf-4585-bbbe-307aabcd1363',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FAPB%20MP3.mp3?alt=media&token=3a4dbc5a-eb6a-4cf8-81ab-da9e454e4835', 
      artist: 'Ken Washington',
      title: 'Absolutely Positively Beautiful',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
      tags: ['valentines', 'birthday'],
      purchased: false // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
      
    },
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMy%20Music.wav?alt=media&token=efcf735f-9208-47ad-810c-9d170e06fa36',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Music.mp3?alt=media&token=aeb156ab-ae36-44a5-81b1-720f8947dc12', 
      artist: 'Ken Washington',
      title: 'My Music',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
      tags: ['wedding', 'just because'] ,
      purchased: false// tell the plugin to preload metadata such as duration for this track,  set to 'none' to turn off
    }, 
    {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FReal%20MP3.mp3?alt=media&token=7a42ed06-f974-4064-94c3-3307f21f5346',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FReal%20MP3.mp3?alt=media&token=fb731d4c-eb41-49e5-a5d6-f183a2893d6b', 
      artist: 'Ken Washington',
      title: 'Real',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
       tags: ['just because', 'valentines'] ,
      purchased: false
       // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    },
     {
      src: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Snippets%2FMyDesire.mp3?alt=media&token=4d2e2897-200b-481b-92ac-52309885034c',
      fullsrc: 'https://firebasestorage.googleapis.com/v0/b/e-luv-7b06b.appspot.com/o/Songs%2FMy%20Desire%20MP3.mp3?alt=media&token=1fc812ad-2f1f-4221-864d-106bfe0e6ad8', 
      artist: 'Ken Washington',
      title: 'My Desire',
      art: 'https://cdn4.iconfinder.com/data/icons/love-and-romance-two-color-vol-3/512/Happiness__headphonewithheart__love__loveinspiration__lovemusic-128.png',
      preload: 'metadata',
       tags: ['valentines', 'wedding'],
      purchased: false // tell the plugin to preload metadata such as duration for this track, set to 'none' to turn off
    }];

    this.filteredtracks = this.myTracks;
    // this.fbd.getsnippettags().then(songtags => {
  
    //         alert(JSON.stringify(this.snippetags));
    this.initializeItems();
         
    // });
  }

  ngAfterContentInit() {     
    // get all tracks managed by AudioProvider so we can control playback via the API
    this.allTracks = this._audioProvider.tracks; 
    this.grid = Array(Math.ceil(this.holidayItems.length/2));

    let rowNum = 0; //counter to iterate over the rows in the grid
    

    for (let i = 0; i < this.holidayItems.length; i+=2) { //iterate images
      this.counter = i;
      this.grid[rowNum] = Array(2); //declare two elements per row
  
      if (this.holidayItems[i].avatarUrl) { //check file URI exists
        this.grid[rowNum][0] = this.holidayItems[i] //insert image
      }
  
      if (this.holidayItems[i+1]) { //repeat for the second image
        this.grid[rowNum][1] = this.holidayItems[i+1]
      }
  
      rowNum++; //go on to the next row
      
    }
  
  }


  
  playSelectedTrack() {
    // use AudioProvider to control selected track 
    this._audioProvider.play(this.selectedTrack);
  }
  
  pauseSelectedTrack() {
     // use AudioProvider to control selected track 
     this._audioProvider.pause(this.selectedTrack);
  }
         
  onTrackFinished(track: any) {
   // alert('Track finished: ' + track)
  } 

  purchase(track: any ) {
    this.purchasemodal(track).then((data: any) => {
      if(data) {
          track.purchased = true; 
      var elem = this.myTracks.findIndex(p => p.src === track.src);
   //  alert("purchase simulation for track "+ track.title + JSON.stringify(elem));
      return true; 
      }
    });
  
  }

  sendmodal(track: any) {
   
    return new Promise((resolve, reject) => {
         //  alert('load modal w/ params: ' + JSON.stringify(track));
          let sendsongModal = this.modalCtrl.create(SendModal, { track: track });
          sendsongModal.onDidDismiss(data => {
            if(data) {
              resolve(data);
            }          
          }); 
          sendsongModal.present();

      });
  }

   purchasemodal(track: any) {
   
    return new Promise((resolve, reject) => {
         //  alert('load modal w/ params: ' + JSON.stringify(track));
          let purchModal = this.modalCtrl.create(PurchaseModal, { track: track });
          purchModal.onDidDismiss(data => {
            if(data) {
              resolve(data);
            }          
          }); 
          purchModal.present();

      });
  }

  holidaymodal(holiday: any, icon: any) {
     return new Promise((resolve, reject) => {
        //  alert('load modal w/ params: ' + JSON.stringify(holiday));
          let holidayModal = this.modalCtrl.create(HolidayDetailModal, { holiday: holiday, icon: icon });
          holidayModal.onDidDismiss(data => {
            if(data) {
              resolve(data);
            }          
          }); 
          holidayModal.present();

      });
  }

 
  initializeItems() {
    this.tags = ['wedding', 'valentines', 'just because', 'birthday'];

    if(this.filteredtracks.length > 0 ) {
   //   alert("made it inside if");
      var counter = 0; 
      this.filteredtracks.forEach(element => {
        counter++; 
     //   alert("saving purchase information for track: " +  counter + element.purchased); 
      });
    }

    this.filteredtracks = this.myTracks;
  
  }

  getItems(ev: any) {
    // Reset items back to all of the items
   // alert('event params: ' + JSON.stringify(ev));
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.toString();
    var filt = []; 
    this.myTracks.forEach(function(item, index) {
      var count = 0; 
      item.tags.forEach(function(item, index) {
       // alert('ev = ' + JSON.stringify(ev) + ' : ' + item);
        
        if(val.toLowerCase().includes(item)) {
          count++; 
        }
      });

      if(count > 0) {
        filt.push(item);
      }
      
    });

   // alert('filtered songs' + JSON.stringify(filt));
    
    if(filt.length > 0) {
       this.filteredtracks = filt; 
    }
    else {
      this.filteredtracks = this.myTracks; 
    }
   

    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.tags = this.tags.filter((item) => {
    //     this.filteredsnippetags = this.filtersnippets(val);
    //     return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
    //   })
    // }
  }

  filtersnippets(tag: any) {
    var temparr = [];
    if (tag && tag.trim() != '') {
      this.filteredsnippetags.forEach(element => {
        if (element.tags.includes(tag)) {
          temparr.push(element);
        }
      });

      // alert(JSON.stringify('filtered items' + temparr));
      return temparr;

    }
  }

  // play(item: any ) {
  //  // alert('playing item: ' + item.filename);
  //   this.fbd.getsnippeturl(item.filename).then(url => {
  //  //    alert('snippet download url: ' + url);
  //      var audio = new Audio();
  //       audio.src = url;
  //       audio.load();
  //       audio.play();
  //   });
  // }

  itemTapped(event, item) {
    this.navCtrl.push(ItemDetailsPage, {
      item: item
    });
  }

  loadHoliday(holiday) {

  }

  
}
