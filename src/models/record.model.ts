export class RecordInfo  {
  public datecreated: string;
    public datemodified: string;
    public userid: string;
    public accountid: string;
    public processed: boolean;
    public filename: string;
    public recid: string;  
  
  constructor(user: any, file: any) 
  {
    if (user != null && file != null)
    {
      this.filename = file.name;
      this.userid = user.userid;
      this.accountid = user.accountid;
      this.datecreated = new Date().toUTCString(); 
      this.datemodified = new Date().toUTCString();
      this.processed = false;  
      this.recid = null; 
      
    }
    else
    {
      this.filename = null;
      this.userid = null;
      this.accountid = null;
      this.datecreated = new Date().toUTCString(); 
      this.datemodified = new Date().toUTCString();
      this.processed = false;  
      this.recid = null; 
    }
   }
};