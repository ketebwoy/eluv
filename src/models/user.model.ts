export class UserInfo {

  constructor(
    public datecreated: string,
    public datemodified: string,
    public email: string,
    public phone: string,
    public fullname: string,
    public profilepic: string,
    public userid: string,
    public accountid: string,
    public role: string
  ) {
     if (accountid == null)
                    {
                    this.datecreated = new Date().toUTCString(); 
                    this.datemodified = new Date().toUTCString(); 
                    this.profilepic = "http://www.gravatar.com/avatar?d=mm&s=140";
                    this.userid = ''; 
                    this.accountid = '';
                    this.fullname = '';
                    }
  }
};