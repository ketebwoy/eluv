import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import 'rxjs/add/operator/share';
//import { UserInfo } from '../models/user.model';
//import { LocalStorage } from '../services/storage';
//import { Storage } from '@ionic/storage';
import { FireBaseData } from '../services/firebase.service';
//import * as moment from 'moment';
import { Platform } from 'ionic-angular';
import { Toast } from 'ionic-native';
import { File } from 'ionic-native';
//import * as _ from 'underscore/underscore'


declare var window: any;
declare var cordova: any;



@Injectable() export class AppGlobals {
    // use this property for property binding
    private _isUserLoggedIn = new BehaviorSubject<boolean>(false);

    //private _user = new BehaviorSubject<UserInfo>(new UserInfo(null, null, null, null, null, null, null, null, null));

    // private _company = new BehaviorSubject<CompanyInfo>(new CompanyInfo(null, null, null, null, null, null, null, null, null, null, null, null));
    private _observer: any;
    private _userobserver: any;

    loggedinstatus$ = this._isUserLoggedIn.asObservable();
  //  user$ = this._user.asObservable();
    //company$ = this._company.asObservable();
    localstorage: any;
    storage: Storage;
    recordlist: any;
    records: any;
    fbd: any;
    file: any;

    constructor(private platform: Platform) {

        // this.loggedinstatus$ = new Observable(observer => this._observer = observer).share();
        // this.user$ = new Observable(_userobserver => this._userobserver = _userobserver).share();
        // this.fbd = new FireBaseData();
        // this.localstorage = new LocalStorage(this.storage);
        // setInterval(() => {
        //     this.getrecordlist();
        // }, 60000);
    }

}

//     setloginstatus(isLoggedIn: boolean) {
//         this._isUserLoggedIn.next(isLoggedIn);
//     }


//     setuser(userarg: any) {
//         this._user.next(userarg);
//     }


//     getrecordlist() {
//         this.localstorage.getsettings().then(settingsid => {
//             if (settingsid != '' || settingsid != null) {
//                 this.fbd.getsettingsbyid(settingsid).then(data2 => {
//                     if (data2.settingsid == settingsid) {
//                         this.localstorage.getrecordslist().then((list: any) => {
//                             this.recordlist = list;
//                             if (this.recordlist != null) {
//                                 this.records = this.recordlist.records;
//                                 for (var key in this.recordlist.records) {
//                                     //   alert('trying to pull created value to evaluate: ' +  this.recordlist.records[key].datecreated );                
//                                     var now = moment();
//                                     var mom2 = moment(this.recordlist.records[key].datecreated).add(data2.timer, 'hours');
//                                     var diff = mom2.diff(now, 'seconds');
//                                     // alert('time difference = : ' + diff + ' for record: ' + this.recordlist.records[key].fname);
//                                     if (diff <= 0 && this.recordlist.records[key].processed == false) {
                                       
//                                         this.localstorage.getaccount().then(user => {
//                                              //upload file to firebase 
//                                         this.showToast("Uploading File: ", 'top');

//                                             if (user.accountid != '') {
//                                                 this.upload(user, this.recordlist.records[key].fname);
                                              
//                                             }
//                                         });

//                                         //delete origin file 

//                                         //alert user through toast 
//                                     }

//                                 }
//                             }
//                         });

                  
//                     }
//                 });
//             }
//         });
//     }



//     showToast(message, position) {
//         Toast.show(message, "short", position).subscribe(
//             toast => {

//             }
//         );
//     }

//     upload(user: any, fname: any) {
//         //alert("made it audio recorder upload()" + JSON.stringify(user));
//         this.file = { name: fname };
//      //   alert('filename: ' + fname);
//         File.readAsDataURL(cordova.file.externalRootDirectory, fname).then((data: any) => {
//        //     alert('file data' + data);
//             if (data) {

//                 var blob = this.getblob(data, "audio/mpeg3");
//             //    alert('blob ' + JSON.stringify(blob));
//                // alert('fname: ' + fname);
//                 this.fbd.uploadrecording(fname, blob).then((success: any) => {
//                     if (success) {
//                         //alert("made it userrec attempt");
//                         this.fbd.user2recording(this.file, user).then((res: any) => {
//                             if (res) {
//                                   this.markprocessed(fname);
//                             }
//                         });
//                     }
//                 });

//             }
//         });

//     }


//     getblob(dataURI, dataTYPE) {
//         var binary = atob(dataURI.split(',')[1]), array = [];
//         for (var i = 0; i < binary.length; i++) array.push(binary.charCodeAt(i));
//       //  alert(JSON.stringify(array));
//         return new Blob([new Uint8Array(array)], { type: dataTYPE });
//     }

//     markprocessed(fname) {
//        var rec = _.each(this.recordlist.records, function (rec) {
//            if (rec.fname == fname) {
//                 rec.processed = true; 
//                 rec.datemodified = moment().format('LLLL');
              
//             } 
//         });
               
//          this.localstorage.setrecordlist(this.recordlist.records);




//     }


// }