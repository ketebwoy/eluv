import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { File } from 'ionic-native';
import { UserInfo } from '../models/user.model';
//import { RecordInfo } from '../models/record.model';
//import {Settings} from '../models/user.settings';
import { Observable } from 'rxjs/Observable';
//import { UserSettings } from '../../models/settings.model';


// firebase
declare var firebase: any;
declare var cordova: any;
//var storageRef = firebase.storage.ref("records/recording.mp3");

@Injectable()
export class FireBaseData {

  public userfb: any;
  public tagsfb: any;
  file: any;
  recordstorage: any;
  public snippetstorage: any;
  public songstorage: any;
  public snippettagsfb: any; 
  public schedulesfb: any;
  public userdata: any;
  public userrecs: any;
  //public settingsdata: any;
  //public settingsobj: any;  

  public usersettings: any;


  constructor() {
    // this.settingsobj = new Settings(); 
    // var rec = firebase.storage();
    // var recref = rec.ref();
    // this.snippetstorage = recref.child("Snippets/");
    // this.songstorage = recref.child("Songs/");
    
    // this.snippettagsfb = firebase.database().ref('snippettags');
    // this.tagsfb = firebase.database().ref('tags');
    // this.schedulesfb = firebase.database().ref('schedules');
  
  }

  creatsongsnippets() {
    //alert("made it to user settings");
    return new Promise((resolve, reject) => {

      var snippettag = {filename: "Real MP3.mp3", tags: "valentine, wedding"}
      alert(JSON.stringify(snippettag));
      this.snippettagsfb.push(snippettag).then((data: any) => {
        alert("tag push success: " + JSON.stringify(data));
        resolve(data);
      }).catch(function (error: any) {
        alert('something went wrong: ' + JSON.stringify(error));//log sommething
        reject(error);
      });
    });
  }


    getsnippettags() {
    //alert("made it to firebase with parameters: " + JSON.stringify(userid));
    return new Promise((resolve, reject) => {
      var songtags = []; 
      this.snippettagsfb.once("value", function (snapshot: any) { //.equalTo(company.companyid).once("value", function (snapshot: any) {
        var sniptagdata = snapshot.child("/").val();

         snapshot.forEach(function(childSnapshot) {

                var key = childSnapshot.key; 
                var data = childSnapshot.val();
                songtags.push(data);
                        
            });
         resolve(songtags);
      });
    });

  }
  
 getdownloadurl(filename) {
   alert('trying to get dl url for: ' + JSON.stringify(filename));
    return new Promise((resolve, reject) => {
      var recsfile = this.snippetstorage.child(filename);
      var url = recsfile.getDownloadURL();
      alert(JSON.stringify(url));
      resolve(url);
      
    });
   }

  getsnippeturl(filename: any) {
    //alert('made it to snippet url function: ' + filename);
     return new Promise((resolve, reject) => {
     var recsfile = this.snippetstorage.child(filename);
    
      recsfile.getDownloadURL().then(function(url) {
        alert('download url ' + url);
         resolve(url);
       }).catch(error => {
         alert(JSON.stringify(error));
       });
       
      
});

  }

   createsongschedule(songschedule) {
    //alert("made it to user settings");
    return new Promise((resolve, reject) => {
      //alert(JSON.stringify(settings));
      this.schedulesfb.push(songschedule).then((data: any) => {
        //alert("settings push success")
        resolve(data);
      }).catch(function (error: any) {
        //log sommething
        reject(error);
      });
    });
  }


   updateschedule(songschedule) {
    // Save user profile     
   // alert('update user settings: ' + JSON.stringify(settings));
    songschedule.datemodified = new Date().toUTCString();
    this.schedulesfb.child(songschedule.scheduleid).update(songschedule);
  }


  // ///////////////////////////////////////////////CRUD OPERATIONS////////////////////////////////////////////////////////////
  // public uploadrecording(file: any, blob: any) {
  //   return new Promise((resolve, reject) => {
  //     //alert("firebase upload: " + JSON.stringify(blob));

  //     // Create file metadata including the content type
  //     // StorageMetadata metadata = new StorageMetadata.Builder()
  //     //   .setContentType("image/jpg")
  //     //   .build();

  //     var uploadTask = this.recordstorage.child(file).put(blob);

  //     // Listen for state changes, errors, and completion of the upload.
  //     uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
  //       function (snapshot) {

  //         // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
  //         var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
  //         //  console.log('Upload is ' + progress + '% done' + ": " + snapshot.bytesTransferred + " / " + snapshot.totalBytes);
  //         switch (snapshot.state) {
  //           case firebase.storage.TaskState.PAUSED: // or 'paused'
  //             console.log('Upload is paused');
  //             break;
  //           case firebase.storage.TaskState.RUNNING: // or 'running'
  //             console.log('Upload is running');
  //             alert("media uploading to firebase");
  //             break;
  //         }
  //       }, function (error) {
  //         switch (error.code) {
  //           case 'storage/unauthorized':
  //             reject(error);
  //             //console.log(JSON.stringify(error));
  //             break;

  //           case 'storage/canceled':
  //             reject(error);
  //             //console.log(JSON.stringify(error));
  //             // //alert("cancelled: " + error.serverResponse);
  //             // User canceled the upload
  //             break;


  //           case 'storage/unknown':
  //             //console.log(JSON.stringify(error));
  //             reject(error);
  //             ////alert("error: " + JSON.stringify(error));
  //             // Unknown error occurred, inspect error.serverResponse
  //             break;
  //         }
  //       }, function () {

  //         // Upload completed successfully, now we can get the download URL
  //         var downloadURL = uploadTask.snapshot.downloadURL;
  //         resolve(true);
  //       });
  //   });

  // }

  // public user2recording(file: any, user: any) {
  //   //alert("made it to record user association");
  //   return new Promise((resolve, reject) => {
  //     var rec = new RecordInfo(user, file);
  //     //alert(JSON.stringify(rec));
  //     this.userrecs.push(rec).then((data: any) => {
  //       //alert("rec push success")
  //       resolve(data);
  //     }).catch(function (error: any) {
  //       //log sommething
  //       reject(error);
  //     });
  //   });

  // }

  getrecordings(userid: any) {
    //alert("made it to firebase with parameters: " + JSON.stringify(userid));
    return new Promise((resolve, reject) => {
      this.userrecs.orderByChild("userid").once("value", function (snapshot: any) { //.equalTo(company.companyid).once("value", function (snapshot: any) {
        var recdata = snapshot.child("/").val();

        resolve(recdata);
      });
    });

  }

  // createusersettings(settings) {
  //   alert("made it to user settings");
  //   return new Promise((resolve, reject) => {
  //     alert(JSON.stringify(settings));
  //     this.usersettings.push(settings).then((data: any) => {
  //       alert("settings push success")
  //       resolve(data);
  //     }).catch(function (error: any) {
  //       log sommething
  //       reject(error);
  //     });
  //   });
  // }


  // getusersettings(userid) {
  //   alert(userid);
  //   var settings = new Settings();
  //   return new Promise((resolve, reject) => {
  //     this.usersettings.orderByChild("userid").equalTo(userid).once("value", function (snapshot: any) { //.equalTo(company.companyid).once("value", function (snapshot: any) {
  //       var recdata = snapshot.child("/").val();
  //       snapshot.forEach(function (childSnapshot) {
  //         var key = childSnapshot.key;
  //         var data = childSnapshot.val();
  //         //alert(key);
  //         settings.contact1 = data.contact1;
  //         settings.contact2 = data.contact2;
  //         settings.contact3 = data.contact3;
  //         settings.contact4 = data.contact4;
  //         settings.contact5 = data.contact5;
  //         settings.timer = data.timer;
  //         settings.userid = data.userid;
  //         settings.settingsid = data.settingsid;
  //       });

  //       alert(JSON.stringify( settings.settingsid ));
  //       resolve(settings);
  //     });
  //   });
  // }

  // getsettingsbyid(settingsid) {
  //   alert(userid);
  //   var settings = new Settings();
  //   return new Promise((resolve, reject) => {
  //     this.usersettings.orderByChild("settingsid").equalTo(settingsid).once("value", function (snapshot: any) { //.equalTo(company.companyid).once("value", function (snapshot: any) {
  //       var recdata = snapshot.child("/").val();
  //       snapshot.forEach(function (childSnapshot) {
  //         var key = childSnapshot.key;
  //         var data = childSnapshot.val();
  //         //alert(key);
  //         settings.contact1 = data.contact1;
  //         settings.contact2 = data.contact2;
  //         settings.contact3 = data.contact3;
  //         settings.contact4 = data.contact4;
  //         settings.contact5 = data.contact5;
  //         settings.timer = data.timer;
  //         settings.userid = data.userid;
  //         settings.settingsid = data.settingsid;
  //         settings.length = data.length;
  //       });

  //       alert(JSON.stringify( settings ));
  //       resolve(settings);
  //     });
  //   });
  // }


  // updateusersettings(settings: Settings) {
  //   Save user profile     
  //   alert('update user settings: ' + JSON.stringify(settings));
  //   this.usersettings.child(settings.settingsid).update(settings);
  // }


  ////////////////////////////////////////////////SIGN UP FUNCTIONS ////////////////////////////////////////////////////////
  getuser(userid: string) {
    return new Promise((resolve, reject) => {
      this.userdata.orderByChild("userid").equalTo(userid).once("value", function (snapshot: any) { //.equalTo(company.companyid).once("value", function (snapshot: any) {
        var recdata = snapshot.child("/").val();
        snapshot.forEach(function (childSnapshot) {
          var key = childSnapshot.key;
          var data = childSnapshot.val();

          //alert(JSON.stringify(data));
          resolve(data);

        });
      });
    });
  }

  createuser(user: UserInfo) {
    return new Promise((resolve, reject) => {
      user.datecreated = new Date().toUTCString();
      user.datemodified = new Date().toUTCString();
      //alert(JSON.stringify(user));
      this.userdata.push(user).then((data: any) => {
        resolve(data);
      }).catch(function (error: any) {
        //log sommething
        reject(error);
      });
    });

  }

  updateuser(user: UserInfo) {
    // Save user profile     
    user.datemodified = new Date().toUTCString();
    this.userdata.child(user.userid).update(user);
  }

  validateuser(user: UserInfo) {
    var validated: boolean;

    return new Promise((resolve, reject) => {
      this.userdata.orderByChild("accountid").equalTo(user.accountid).once("value", function (snapshot: any) {
        if (snapshot) {
          var accountId = JSON.stringify(snapshot.child("/" + user.userid + "/accountid").val());
          if (accountId == JSON.stringify(user.accountid)) {
            validated = true;
          }
          else {
            validated = false;
          }
        }
        resolve(validated);
      });
    });
  }



  createaccount(credentials: any) {
    return new Promise(function (resolve, reject) {
      firebase.auth().createUserWithEmailAndPassword(credentials.email, credentials.password)
        .then(function (user: any) {
          alert("created user" + JSON.stringify(user))
          resolve(user);
        }).catch(function (error: any) {
          alert("problem creating user" + JSON.stringify(error))
          reject(error);

        });
    });
  }

  

  logout() {
    return new Promise(function (resolve, reject) {
      firebase.auth().signOut().then(function () {
        alert('logout success');
        resolve(true);
      }, function (error) {
        reject(error)
      });
    });
  }

  login(credentials: any) {
    return new Promise(function (resolve, reject) {
      firebase.auth().signInWithEmailAndPassword(credentials.email, credentials.password)
        .then(function (user: any) {
          alert("logged in user" + JSON.stringify(user))
           resolve(user);
        }).catch(function (error: any) {
          alert("logged in user error" + JSON.stringify(error))
           reject(error);

        });
    });
  }

  initializeAccountData() {
    // create user with admin role 

    // create company 
  }

}