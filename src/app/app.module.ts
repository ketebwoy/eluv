import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HelloIonicPage } from '../pages/hello-ionic/hello-ionic';
import { ItemDetailsPage } from '../pages/item-details/item-details';
import { ListPage } from '../pages/list/list';
import { Storage } from '@ionic/storage';
import { AppGlobals } from '../services/globals';
import { IonicAudioModule } from 'ionic-audio';
import { SendModal } from '../modals/sendmodal'; 
import { PurchaseModal} from '../modals/purchasemodal';
import { HolidayDetailModal} from '../modals/holidaydetailmodal';
import { Welcome } from '../pages/welcome/welcome';
import { Login } from '../pages/login/login';
import { Signup } from '../pages/signup/signup';
import { Artists} from '../pages/artist/artist';
import { Account } from '../pages/account/account';
import { Calendar } from '../pages/calendar/calendar';
import { NgCalendarModule  } from 'ionic2-calendar';


@NgModule({
  declarations: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    Artists,
    ListPage,
    SendModal,
    PurchaseModal,
    HolidayDetailModal,
    Welcome,
    Login,
    Signup, 
    Account,
    Calendar

  ],
  imports: [
    IonicModule.forRoot(MyApp), 
    IonicAudioModule.forRoot(),
    NgCalendarModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HelloIonicPage,
    ItemDetailsPage,
    ListPage,
    SendModal,
    PurchaseModal,
    Welcome,
    Login,
    Signup,
    Artists,
    HolidayDetailModal,
    Account,
    Calendar
  ],
  providers: [AppGlobals, Storage, {provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
